#ifndef MENU_H
#define MENU_H

#include "raylib.h"
#include "game/objects/button/button.h"

using namespace match3;

namespace match3 {

	namespace menu {

		void init();
		void update();
		void draw();
		void deinit();
	}
}
#endif