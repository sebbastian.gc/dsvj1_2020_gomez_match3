#include "gameplay.h"
#include "game/input/input.h"
#include "game/objects/character/character.h"

namespace match3 {
	namespace gameplay {

		struct matchList{
			short x = -1;
			short y = -1;
			short id = -1;
			bool active = true;
		};

		short typesJewel;
		short randJewel;
		short matchId;
		short matchPos;
		const short sizeMatch=20;
		const short match = 3;
		bool firts;	
		bool gameover;
		bool win;
		bool pause;
		float   volume;
		short enemyLife;
		short life;		
		short limitMoves;
		short time;

		Sound     fxButton;
		Sound     fxjewel;
		Sound     fxmatch;
		Music     gameplayMusic;

		matchList list[sizeMatch];

		character* player;
		character* enemy;
		jewel* jewels[size][size];		
		Texture2D backTexture;
		Texture2D pauseTexture;
		Texture2D gameOverTexture;
		Texture2D winTexture;
		Texture2D background;
		Texture2D jewelTexture[4];


		button* back;
		button* _pause;

		void static reboot() {
			StopMusicStream(gameplayMusic);			
			currentScene = SCENES::MENU;			
			initscenes = true;	
			pause = false;
			deinit();
		}
		void static buttonBack() {
			back->update();
			if (back->getButtonAction()) {
				reboot();
			}
		}

		void static buttonPause() {
			_pause->update();
			if (_pause->getButtonAction()) {
				
				if (!pause) pause = true;
				else pause = false;
			}
		}

		void static spawnJewels() {
			short x = 120;
			short y = 420;
			short offsetX = 5;
			short offsetY = 10;

			for (short i = 0; i < size; i++) {
				for (short j = 0; j < size; j++) {
					jewels[i][j] = NULL;
				}
			}
			for (short i = 0; i < size; i++) {
				for (short j = 0; j < size; j++) {
					randJewel = rand() % (typesJewel);
					if (j == 0) {
						jewels[i][j] = new jewel(jewelTexture[randJewel], static_cast<float>(x), static_cast<float>(y + ((jewelTexture[randJewel].height + offsetY) / 2 * i)), randJewel);
					}
					else jewels[i][j] = new jewel(jewelTexture[randJewel], static_cast<float>(x + ((jewelTexture[randJewel].width + offsetX) * j)), static_cast<float>(y + ((jewelTexture[randJewel].height + offsetY) / 2 * i)), randJewel);
				}
			}
		}
		bool static limits(short x, short y) {			
			float i = list[matchPos].x;
			float j = list[matchPos].y;
			if ((i - 1 == x || i + 1 == x || i == x) && (j - 1 == y || j + 1 == y || j == y)) {
				return true;
			}
			else return false;			
		}
		bool static previous(short x, short y) {
			short aux = 0;
			for (short i = 0; i < sizeMatch; i++){				
					if (list[i].active) {						
						if (list[i].x == x && list[i].y == y) {							
							aux--;
						}
						else aux++;					
					}				
			}
			if (aux == matchPos + 1) {
				aux = 0;
				return true;
			}
			else return false;
		}	
		void static setList(short _x, short _y, short _id, bool _active, short i) {
			list[i].x = _x;
			list[i].y = _y;
			list[i].id = _id;
			list[i].active = _active;
		}
		void static reSpawnJewels() {
			for (short i = 0; i < size; i++) {
				for (short j = 0; j < size; j++) {
					if (jewels[i][j] != NULL) {
						if (i==0) {
							if (!jewels[i][j]->getActive()) {
								randJewel = rand() % (typesJewel);
								jewels[i][j]->setID(randJewel);
								jewels[i][j]->setTexture(jewelTexture[randJewel]);
								jewels[i][j]->setActive(true);
							}
						}
					}
				}
			}

		}
		void static repositionJewels() {
			for (short i = 0; i < size; i++) {
				for (short j = 0; j < size; j++) {
					if (jewels[i][j] != NULL) {
						if (i < size-1) {
							if (!jewels[i+1][j]->getActive()) {								
								jewels[i + 1][j]->setTexture(jewels[i][j]->getTexture());
								jewels[i + 1][j]->setID(jewels[i][j]->getID());
								jewels[i + 1][j]->setActive(true);

								jewels[i][j]->setActive(false);								
							}
						}
					}
				}
			}
		}
		void static cleanMatch(){
			if (matchPos +1  >= match) {
				for (short i = 0; i < sizeMatch; i++){
					PlaySound(fxmatch);				
					if (list[i].active) {
						if (list[i].id == 3) {
							life++;
						}
						else {
							player->attack();
							enemyLife -= match;

						}
						
						if (enemyLife < 0) enemyLife = 0;
						if (life > 100) life = 100;

						jewels[list[i].x][list[i].y]->setActive(false);
					}			
				}
			}
			for (short i = 0; i < sizeMatch; i++) {
				setList(-1, -1, -1, false, i);
			}		
		}
		bool static deselect(short x, short y) {
			if (list[matchPos-1].x == x && list[matchPos-1].y == y) {
				return true;
			}
			else return false;
		}
		void static updateJewels() {			
			for (short i = 0; i < size; i++) {
				for (short j = 0; j < size; j++) {					
					if (jewels[i][j] != NULL) {	
						if (jewels[i][j]->getActive()) {
							jewels[i][j]->update();							
							if (jewels[i][j]->getJewelAction()) {
								if (firts) {
									PlaySound(fxjewel);									
									matchId = jewels[i][j]->getID();
									jewels[i][j]->setJewelState(1);
									setList(i, j, jewels[i][j]->getID(), true, matchPos);
									firts = false;									
								}
								else if (deselect(i,j) && matchPos>0 ) {									
									jewels[list[matchPos].x][list[matchPos].y]->setJewelState(0);
									setList(-1, -1, -1, false, matchPos);
									matchPos--;
									std::cout << "el resultaod es " << matchPos << std::endl;
								}
								else if (jewels[i][j]->getID() == matchId && limits(i, j) && previous(i, j)) {
									PlaySound(fxjewel);										
									jewels[i][j]->setJewelState(1);
									matchPos++;
									setList(i, j, jewels[i][j]->getID(), true, matchPos);									
								}
							}
							if (!IsMouseButtonDown(MOUSE_LEFT_BUTTON)) {
								cleanMatch();								
								firts = true;								
								matchId = -1;
								matchPos = 0;								
							}
						}						
					}
				}
			}			
		}
		void static Win() {
			if (life > 0 && enemyLife <= 0) {
				win = true;
			}
		}
		void static gameOver() {
			if (life <= 0 && enemyLife > 0) {
				gameover = true;
			}
		}
		void static music() {
			UpdateMusicStream(gameplayMusic);
			if (input::mute) { volume = 0; }
			else volume = 0.2f;
			SetMusicVolume(gameplayMusic, volume);
			PlayMusicStream(gameplayMusic);

		}		
		void static drawJewels() {
			for (short i = 0; i < size; i++) {
				for (short j = 0; j < size; j++) {
					if (jewels[i][j] != NULL) {
						if (jewels[i][j]->getActive()) {
							jewels[i][j]->draw();
						}						
					}
				}
			}
		}
		void static drawScore() {
			DrawText(TextFormat("%01i",enemyLife), static_cast<int>(screenWidth - 90.0f), static_cast<int>(screenHeight /1.6f), 40, ORANGE);
			DrawText(TextFormat("%01i", life), static_cast<int>(30.0f), static_cast<int>(screenHeight / 1.6f), 40, ORANGE);
		}
		void static drawWin() {
			if (win) {
				DrawTexture(winTexture, 0, 0, WHITE);
			}
		}
		void static drawgameOver() {
			if (gameover) {
				DrawTexture(gameOverTexture, 0, 0, WHITE);
			}
		}

		void static timeToAttack(short t) {
			time++;
			if (time / 60 >= t) {
				time = 0;
				enemy->attack();
				life -= 10;
				if (life < 0) life = 0;
			}
		}
		
		void init() {			
			typesJewel = 4;
			randJewel = 0;
			matchId = -1;	
			time = 0;
			backTexture = LoadTexture("res/assets/button_back.png");
			pauseTexture = LoadTexture("res/assets/button_pause.png");
			background= LoadTexture("res/assets/gameplay.png");
			gameOverTexture = LoadTexture("res/assets/game_over.png");
			winTexture= LoadTexture("res/assets/win.png");
			fxjewel = LoadSound("res/fx/jewel.mp3");
			fxButton = LoadSound("res/fx/button.mp3");
			fxmatch = LoadSound("res/fx/match.mp3");
			gameplayMusic= LoadMusicStream("res/fx/gameplay.mp3");			
			
			back = new button(backTexture, static_cast<float>(screenWidth / 2.3f), 30.0f, fxButton);
			_pause = new button(pauseTexture, static_cast<float>(-250), 30.0f, fxButton);
			player = new character(1);
			enemy = new character(2);


			for (short i = 0; i < typesJewel; i++){
				if (i == 0) {
					jewelTexture[i] = LoadTexture("res/assets/one.png");
				}
				if (i == 1) {
					jewelTexture[i] = LoadTexture("res/assets/two.png");
				}
				if (i == 2) { 
					jewelTexture[i] = LoadTexture("res/assets/three.png");
				}
				if (i == 3) {
					jewelTexture[i] = LoadTexture("res/assets/four.png");
				}
			}
			for (short i = 0; i < sizeMatch; i++){
				list[i].active = false;
			}
			
			spawnJewels();			
			firts = true;			
			initscenes = false;
			gameover = false;
			win = false;
			volume = 0.2f;
			enemyLife = 200;
			life = 100;			
			limitMoves = 100;
		}

		void update() {
			if (back != NULL && _pause != NULL) {
				
				buttonPause();

				if (!pause) {
					music();
					if (!win && !gameover) {
						updateJewels();
						player->update();
						enemy->update();
						timeToAttack(6);
					}
					repositionJewels();
					reSpawnJewels();
					Win();
					gameOver();
				}				
				buttonBack();				
			}
		}

		void draw() {
			if (back != NULL && _pause != NULL) {
				DrawTexture(background, 0, 0, RAYWHITE);
				drawJewels();
				drawScore();
				drawWin();
				drawgameOver();
				back->draw();
				_pause->draw();
				player->draw();
				enemy->draw();
			}
		}
		void deinit() {
			UnloadTexture(background);
			UnloadTexture(winTexture);
			UnloadTexture(gameOverTexture);

			delete player;
			player = NULL;

			delete enemy;
			enemy = NULL;

			delete back;
			back = NULL;

			delete _pause;
			_pause = NULL;

			for (short i = 0; i < typesJewel; i++) {
				 UnloadTexture(jewelTexture[i]);				
			}			
			for (short i = 0; i < size; i++) {
				for (short j = 0; j < size; j++) {						
					delete jewels[i][j];
					jewels[i][j] = NULL;
					
				}
			}

			UnloadSound(fxjewel);
			UnloadSound(fxButton);
			
		}
	}
}