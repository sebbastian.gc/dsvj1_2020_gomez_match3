#include "menu.h"
#include "game/input/input.h"

#include "game/globales.h"

using namespace match3;

SCREENS match3::currentScreen;

namespace match3 {
	namespace menu {

		Texture2D screenMenuTexture;
		Texture2D screenCreditsTexture;	
		Texture2D screenInstTexture;

		Texture2D playTexture;
		
		Texture2D creditsTexture;
		Texture2D exitTexture;
		Texture2D backTexture;
		Texture2D startTexture;
		Music     menuMusic;
		Sound     fxButton;

		button* play;		
		button* credits;
		button* exit;
		button* back;
		button* start;

		float   volume;				
		
		void static playButton() {
			if (play != NULL) {
				play->update();
				if (play->getButtonAction()) {					
					currentScreen = SCREENS::INSTRUCTIONS;
				}
			}
		}
		void static startButton() {
			start->update();
			if (start->getButtonAction()) {				
				currentScene = SCENES::GAMEPLAY;
				initscenes = true;
				deinit();
			}
		}

		void static creditsButton() {
			if (credits != NULL) {
				credits->update();
				if (credits->getButtonAction()) {					
					currentScreen = SCREENS::CREDITS;
				}
			}
			
		}
		void static exitButton() {
			if (exit != NULL) {
				exit->update();
				if (exit->getButtonAction()) {					
					CloseWindow();
				}
			}
		}
		void static backButton() {
			back->update();
			if (back->getButtonAction()) {				
				currentScreen = SCREENS::MENU;
			}
		}
		void static drawMenu() {
			DrawTexture(screenMenuTexture, 0, 0, WHITE);
			play->draw();			
			credits->draw();
			exit->draw();
		}
		
		void static music() {
			UpdateMusicStream(menuMusic);
			if (input::mute) { volume = 0; }
			else volume = 0.2f;
			SetMusicVolume(menuMusic, volume);
			PlayMusicStream(menuMusic); 
			
		}
		void init() {
			currentScreen = SCREENS::MENU;
			currentScene = SCENES::MENU;
			screenMenuTexture = LoadTexture("res/assets/menu.png");
			screenCreditsTexture = LoadTexture("res/assets/screen_credits.png");
			screenInstTexture = LoadTexture("res/assets/start.png");		

			playTexture = LoadTexture("res/assets/button_play.png");			
			creditsTexture = LoadTexture("res/assets/button_credits.png");
			exitTexture = LoadTexture("res/assets/button_exit.png");
			backTexture = LoadTexture("res/assets/button_back.png");
			startTexture = LoadTexture("res/assets/button_start.png");
			
			menuMusic = LoadMusicStream("res/fx/menu.mp3");
			fxButton = LoadSound("res/fx/button.mp3");

			play = new button(playTexture, 40.0f, fxButton);
			credits = new button(creditsTexture, 130.0f, fxButton);
			exit = new button(exitTexture, 220.0f, fxButton);
			start = new button(startTexture, static_cast<float>(screenHeight / 2.5f), fxButton);
			back = new button(backTexture, static_cast<float>(screenWidth / 2.3f), 30.0f, fxButton);
			initscenes = false;
			volume = 0.2f;
		}
		void update() {
			if (play != NULL) {
				music();				
				switch (currentScreen) {
				case SCREENS::MENU:
					playButton();					
					creditsButton();
					exitButton();
					break;
				case SCREENS::CREDITS:
					backButton();
					break;
				case SCREENS::INSTRUCTIONS:
					startButton();
					break;
				}
			}
		}
		void draw() {
			if (play != NULL) {
				switch (currentScreen) {
				case SCREENS::MENU:
					drawMenu();
					break;
				case SCREENS::CREDITS:
					DrawTexture(screenCreditsTexture, 0, 0, WHITE);
					back->draw();
					break;
				case SCREENS::INSTRUCTIONS:
					DrawTexture(screenInstTexture, 0, 0, WHITE);
					start->draw();
					break;
				}
			}
		}
		void deinit() {
			UnloadTexture(screenMenuTexture);	
			UnloadTexture(screenCreditsTexture);
			UnloadTexture(screenInstTexture);
			UnloadMusicStream(menuMusic);

			delete back;
			delete credits;
			delete exit;
			delete play;
			delete start;

			back = NULL;
			credits = NULL;
			exit = NULL;
			play = NULL;
			start = NULL;

			UnloadSound(fxButton);
		}
	}
}