#ifndef CHARACTER_H
#define CHARACTER_H

#include "raylib.h"
#include "game/objects/animator/animator.h"


namespace match3 {

	class character {
	private:
		animator* idleAnim;	
		Texture2D _idleAnim;
		animator* attackAnim;
		Texture2D _attackAnim;
		Vector2 pos;		
		int quantitySprites;
		ANIMATIONS currentAnim;
		
	public:
		character(short _character);
		~character();

		void player();
		void enemy();
		void setPos(float _y);			
		void setFrame(float frame);
		void attack();

		Vector2 getPos();		
		void draw();
		void update();
	};
}
#endif
