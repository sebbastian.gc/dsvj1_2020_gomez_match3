#ifndef GAMELOOP_H
#define GAMELOOP_H

#include "raylib.h"

namespace match3 {

	namespace gameLoop {
		void run();
	}

}
#endif
