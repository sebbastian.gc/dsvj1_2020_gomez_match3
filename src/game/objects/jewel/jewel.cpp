#include "jewel.h"

using namespace match3;

jewel::jewel(Texture2D pad, float posX, float posY, short id) {
	jewel::init(pad, posX, posY,id);
}
jewel::~jewel() {	
	
}
void jewel::init(Texture2D pad, float posX, float posY,short id) {
	ID = id;
	active = true;
	_jewel = pad;	
	numFrames = 2;
	frameHeight = _jewel.height / numFrames;
	sourceRec = { 0, 0, static_cast<float>(_jewel.width), static_cast<float>(frameHeight) };
	jewelBounds = { static_cast<float>( posX), static_cast<float>(posY),static_cast<float>(_jewel.width), static_cast<float>(frameHeight) };
	jewelPos = { jewelBounds.x,jewelBounds.y };
	jewelState = 0;
	jewelAction = false;
	mousePoint = { 0.0f, 0.0f };
}

void jewel::update() {
	mousePoint = GetMousePosition();
	jewelAction = false;
	if (CheckCollisionPointRec(mousePoint, jewelBounds)) {
		if (IsMouseButtonDown(MOUSE_LEFT_BUTTON)){			
			jewelAction = true;			
		}				
	}
	if (!IsMouseButtonDown(MOUSE_LEFT_BUTTON)) {
		jewelState = 0;
		jewelAction = false;
	}
	sourceRec.y = static_cast<float>(jewelState * frameHeight);
}
void jewel::draw() {	
#if _DEBUG
	DrawRectangleLines(static_cast<int>(jewelBounds.x), static_cast<int>(jewelBounds.y), static_cast<int>(_jewel.width), static_cast<int>(_jewel.height/2), RED);
#endif
	DrawTextureRec(_jewel, sourceRec, jewelPos, WHITE);
}
bool jewel::getJewelAction() {
	return jewelAction;
}

bool jewel::getActive() {
	return active;
}
short  jewel::getID() {
	return ID;
}
Texture2D jewel::getTexture() {
	return _jewel;
}
void jewel::setActive(bool _active) {
	active = _active;
}
void jewel::setJewelState(int state) {
	jewelState = state;
}
void jewel::setID(short id) {
	ID = id;
}
void jewel::setTexture(Texture2D pad) {
	_jewel = pad;
}