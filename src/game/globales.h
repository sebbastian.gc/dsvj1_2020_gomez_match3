
#ifndef GLOBAL_H
#define GLOBAL_H

#include <iostream>
#include <stdlib.h>
#include <time.h>

namespace match3 {

	extern int screenWidth;
	extern int screenHeight;
	extern bool initscenes;	
	const  float frameRate = 60.0f;
	const short size = 8;

	enum class SCENES { MENU, GAMEPLAY };
	enum class SCREENS { MENU, CREDITS,  GAMEPLAY, INSTRUCTIONS	};
	enum class ANIMATIONS { IDLE, ATTACK};
	
	extern SCENES currentScene;
	extern SCREENS currentScreen;		
	
}
#endif
