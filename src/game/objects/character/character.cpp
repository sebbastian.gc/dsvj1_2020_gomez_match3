#include "character.h"
#include "game/globales.h"

using namespace match3;

character::character(short _character) {

	switch (_character)
	{
		case 1:
			character::player();
			break;
		case 2:
			character::enemy();
			break;
	}
	
}

character::~character() {

	UnloadTexture(_idleAnim);
	UnloadTexture(_attackAnim);

	delete attackAnim;
	attackAnim = NULL;
	delete idleAnim;	
	idleAnim = NULL;
}
void character::player() {
	_idleAnim = LoadTexture("res/assets/P_Iddle.png");
	_attackAnim = LoadTexture("res/assets/P_Attack.png");
	quantitySprites = 10;
	pos.x = static_cast<float>(190);
	pos.y = static_cast<float>(222);
	idleAnim = new animator(_idleAnim, pos, quantitySprites);
	attackAnim = new animator(_attackAnim, pos, 6);
	currentAnim = ANIMATIONS::IDLE;


}

void character::enemy() {
	_idleAnim = LoadTexture("res/assets/E_Iddle.png");
	_attackAnim = LoadTexture("res/assets/E_Attack.png");
	quantitySprites = 11;
	pos.x = static_cast<float>(300);
	pos.y = static_cast<float>(280);
	idleAnim = new animator(_idleAnim, pos, quantitySprites);
	attackAnim = new animator(_attackAnim, pos, 18);
	currentAnim = ANIMATIONS::IDLE;

}

void character::setPos(float _y) {
	pos.y = _y;
}

void character::setFrame(float frame) {
	idleAnim->setCurrentFrame(frame);
	attackAnim->setCurrentFrame(frame);

}

void character::attack() {
	
	attackAnim->setCurrentAnim(ANIMATIONS::ATTACK);	
}

Vector2 character::getPos() {
	return pos;
}

void character::update() {	

	currentAnim = attackAnim->getCurrentAnim();

	switch (currentAnim) {
		case ANIMATIONS::IDLE:
			idleAnim->update(pos);
			break;
		case ANIMATIONS::ATTACK:
			attackAnim->update(pos);
			break;
	}
}

void character::draw() {	

	switch (currentAnim) {
	case ANIMATIONS::IDLE:
		idleAnim->draw();
		break;
	case ANIMATIONS::ATTACK:
		attackAnim->draw();
		break;
	}
	
}