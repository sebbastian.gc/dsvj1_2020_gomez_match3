#include "gameLoop/gameLoop.h"

using namespace match3;

int main() {
    gameLoop::run();

    return 0;
}