#ifndef JEWEL_H
#define JEWEL_H

#include "raylib.h"
#include "game/globales.h"

namespace match3 {

	class jewel {
	private:
		Texture2D _jewel;		
		int numFrames;
		int frameHeight;
		Rectangle sourceRec;
		Rectangle jewelBounds;
		Vector2 jewelPos;
		int jewelState;
		bool jewelAction;
		Vector2 mousePoint;
		short ID;
		bool active;
		

	public:
		
		jewel(Texture2D pad, float posX, float posY,short id);
		~jewel();
		void init(Texture2D pad, float posX, float posY,short id);
		void update();
		void draw();
		bool getJewelAction();
		bool getActive();
		short getID();
		Texture2D getTexture();		
		void setActive(bool _active);
		void setJewelState(int state);		
		void setID(short id);
		void setTexture(Texture2D pad);

	};
}
#endif

