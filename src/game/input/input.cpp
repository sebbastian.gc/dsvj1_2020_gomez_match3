#include "input.h"
#include "game/globales.h"

using namespace match3;

bool input::mute;

namespace match3 {

	namespace input {

		void init() {
			mute = false;
		}
		void update() {
	
			if (IsKeyPressed(KEY_Q)) { !mute ? mute = true : mute = false; }

		}
	}
}