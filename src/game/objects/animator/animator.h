#ifndef ANIMATOR_H
#define ANIMATOR_H

#include "raylib.h"
#include "game/globales.h"

namespace match3 {
	class animator {
	private:
		Texture2D texture;
		Rectangle rec;
		Vector2 pos;
		int quantitySprites;
		float currentFrame;
		int framesCounter;
		float framesSpeed;
		ANIMATIONS currentAnim;

	public:
		animator(Texture2D anim, Vector2 _pos, int quantity);
		~animator();
		void init();
		void update(Vector2 _pos);
		void setCurrentFrame(float frame);
		void draw();
		ANIMATIONS getCurrentAnim();
		void setCurrentAnim(ANIMATIONS anim);
	};
}
#endif