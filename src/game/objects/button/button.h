#ifndef BUTTON_H
#define BUTTON_H

#include "raylib.h"
#include "game/globales.h"

namespace match3 {

	class button {
	private:
		Texture2D _button;
		Sound fxButton;
		int numFrames;
		int frameHeight;
		Rectangle sourceRec;
		Rectangle buttonBounds;
		Vector2 buttonPos;
		int buttonState;
		bool buttonAction;
		Vector2 mousePoint;

	public:
		button(Texture2D pad, float posY, Sound fx);
		button(Texture2D pad, float posX, float posY, Sound fx);
		~button();
		void init(Texture2D pad, float posX, float posY, Sound fx);
		void init(Texture2D pad, float posY, Sound fx);
		void update();
		void draw();
		bool getButtonAction();
		void fxSound();
	};
}
#endif

