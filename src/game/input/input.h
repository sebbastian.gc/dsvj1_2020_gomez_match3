#ifndef INPUT_H
#define INPUT_H

#include "raylib.h"

namespace match3 {

	namespace input {

		extern bool mute;

		void init();
		void update();
	}
}
#endif
