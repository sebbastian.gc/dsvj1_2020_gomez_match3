#include "gameLoop.h"

#include "game/globales.h"
#include "game/input/input.h"
#include "game/scenes/menu/menu.h"
#include "game/scenes/gamePlay/gamePlay.h"

using namespace match3;

int match3::screenWidth;
int match3::screenHeight;
bool match3::initscenes;
SCENES match3::currentScene;

namespace match3 {
    namespace gameLoop {

        void static init() {
            srand(static_cast<unsigned int>(time(NULL)));
            screenWidth = 600;
            screenHeight = 800;           
            currentScene = SCENES::MENU;
            initscenes = true;
            InitWindow(screenWidth, screenHeight, " ");
            SetTargetFPS(static_cast<int>(frameRate));
            InitAudioDevice();
            input::init();
        }
        void static input() {
            input::update();
        }
        void static update() {
            switch (currentScene) {
            case SCENES::MENU:
                if (initscenes) { menu::init(); }
                menu::update();
                break;
            case SCENES::GAMEPLAY:
                if (initscenes) { gameplay::init(); }
                gameplay::update();
                break;
            }
        }
        void static draw() {
            BeginDrawing();
            ClearBackground(BLACK);
            switch (currentScene) {
            case SCENES::MENU:
                menu::draw();
                break;
            case SCENES::GAMEPLAY:
                gameplay::draw();
                break;
            }
            EndDrawing();
        }
        void static deinit() {
           menu::deinit();
           gameplay::deinit();

        }
        void run() {
            init();
            while (!WindowShouldClose()) {
                input();
                update();
                draw();
            }
            CloseAudioDevice();
            deinit();

        }

    }
}